PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE Locations(
  id number primary key not null,
  coordinates varchar2(5000) not null,
  indate timestamp not null
);
CREATE TABLE panic_requests(
  id number primary key not null,
  indate timestamp not null,
  status char(3) not null
);
CREATE TABLE panic_request_x_location(
  panic_request_id number not null,
  location_id number not null,
  primary key(panic_request_id, location_id) 
);
COMMIT;
