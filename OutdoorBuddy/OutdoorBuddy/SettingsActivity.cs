﻿using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Widget;
using System;

namespace OutdoorBuddy
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class SettingsActivity : AppCompatActivity
    {
        private TextInputEditText textInputAlarmContact;
        private TextInputEditText textInputGoodPin;
        private TextInputEditText textInputBadPin;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_settings);

            var contextPref = Application.Context.GetSharedPreferences(Defines.PreferencesName, Android.Content.FileCreationMode.Private);

            textInputAlarmContact = FindViewById<TextInputEditText>(Resource.Id.textInputEditText1);
            textInputAlarmContact.Text = contextPref.GetString(Defines.PhoneKey, "");

            textInputGoodPin = FindViewById<TextInputEditText>(Resource.Id.textInputGoodPin);
            textInputGoodPin.Text = contextPref.GetString(Defines.GoodPinKey, "0000");

            textInputBadPin = FindViewById<TextInputEditText>(Resource.Id.textInputBadPin);
            textInputBadPin.Text = contextPref.GetString(Defines.BadPinKey, "1111");

            Button button = FindViewById<Button>(Resource.Id.buttonSaveSettings);
            button.Click += OnSaveButtonClicked;

            Button buttonAbortSettings = FindViewById<Button>(Resource.Id.buttonAbortSettings);
            buttonAbortSettings.Click += onAbortButtonClicked;
        }

        private void onAbortButtonClicked(object sender, EventArgs e)
        {
            Finish();
        }

        public static String getGoodPin()
        {
            var contextPref = Application.Context.GetSharedPreferences(Defines.PreferencesName, Android.Content.FileCreationMode.Private);
            return contextPref.GetString(Defines.GoodPinKey, "");
        }

        public static String getBadPin()
        {
            var contextPref = Application.Context.GetSharedPreferences(Defines.PreferencesName, Android.Content.FileCreationMode.Private);
            return contextPref.GetString(Defines.BadPinKey, "");
        }

        private void OnSaveButtonClicked(object sender, EventArgs e)
        {
            var contextPref = Application.Context.GetSharedPreferences(Defines.PreferencesName, Android.Content.FileCreationMode.Private);
            var contextEdit = contextPref.Edit();
            contextEdit.PutString(Defines.PhoneKey, textInputAlarmContact.Text);
            contextEdit.PutString(Defines.GoodPinKey, textInputGoodPin.Text);
            contextEdit.PutString(Defines.BadPinKey, textInputBadPin.Text);
            contextEdit.Commit();

            Finish();
        }
    }
}

