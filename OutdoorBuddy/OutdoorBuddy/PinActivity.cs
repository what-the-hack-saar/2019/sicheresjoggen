﻿using System;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content.PM;
using Android.Gms.Common;
using Android.Gms.Location;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Telephony;

namespace OutdoorBuddy
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class PinActivity : AppCompatActivity
    {
        private const String pinKey = "pin_key";
        private SmsSender sender;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_pin);

            Button checkPinButton = FindViewById<Button>(Resource.Id.buttonStopRunning);
            checkPinButton.Click += onCheckPinClick;
        }

        private void onCheckPinClick(object sender, EventArgs e)
        {
            TextInputEditText textInputPin = FindViewById<TextInputEditText>(Resource.Id.textInputPin);

            if ( textInputPin.Text == SettingsActivity.getGoodPin() )
            {
                Finish();
                return;
            }
            if (textInputPin.Text == SettingsActivity.getBadPin())
            {
                LocationManager locationManager = new LocationManager(this);
                locationManager.GetLocationAndSendSms1();
                Finish();
                return;
            }
            // NO pin matched, give error message
            // TODO
        }
    }
}

