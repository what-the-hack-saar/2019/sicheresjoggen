﻿namespace OutdoorBuddy
{
    public static class Defines
    {
        public const string PreferencesName = "OutdoorBuddy";
        public const string PhoneKey = "phone_key";
        public const string GoodPinKey = "goodpin_key";
        public const string BadPinKey = "badpin_key";
    }
}