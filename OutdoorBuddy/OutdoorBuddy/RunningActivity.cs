﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using System;

namespace OutdoorBuddy
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class RunningActivity : AppCompatActivity
    {
        private TextView emergenyTextView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_running);

            TextView titleText = FindViewById<TextView>(Resource.Id.textViewTitle);
            titleText.Text = UserState.getState().getRunningState();

            Button startAlarmButton = FindViewById<Button>(Resource.Id.startTimer);
            startAlarmButton.Click += onStartAlarmClick;

            Button stopRunningButton = FindViewById<Button>(Resource.Id.stopRunning);
            stopRunningButton.Click += onStopRunningClick;

            Button panicButton = FindViewById<Button>(Resource.Id.runningPanic);
            stopRunningButton.Click += onPanicButtonClick;

            emergenyTextView = FindViewById<TextView>(Resource.Id.textEmergencyText);
        }

        private void onPanicButtonClick(object sender, EventArgs e)
        {
            // TODO wait and send message
            emergenyTextView.Text = "5 Sekunden Timer"; // setzt den Text
        }

        private void onStopRunningClick(object sender, EventArgs e)
        {
            // TODO Frage nach Pin vor schliessen
            Finish();
        }

        private void onStartAlarmClick(object sender, EventArgs e)
        {
            // Wechsel zur TimerUI
            StartActivity(typeof(WarningActivity));
        }
    }
}