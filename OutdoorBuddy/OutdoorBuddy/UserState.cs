﻿using System.Threading;

namespace OutdoorBuddy //OutdoorBuddy
{
    public class UserState
    {
        private bool isRunning = false;
        private static UserState theState = new UserState();
        private string currentRunningState = "Freier Lauf";

        private UserState() //  make singleton
        { }

        public bool getRunning()
        {
            return this.isRunning;
        }

        public void setRunningState(string running)
        {
            currentRunningState = running;
        }

        public string getRunningState()
        {
            return currentRunningState;
        }

        public void setRunning(bool newState)
        {
            try
            {
                Monitor.Enter(this);
                this.isRunning = newState;
            } finally
            {
                Monitor.Exit(this);
            }
            
        }

        public static UserState getState()
        {
            return theState;
        }
    }
}