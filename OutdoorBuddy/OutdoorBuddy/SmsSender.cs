﻿using Android;
using Android.App;
using Android.Content.PM;
using Android.Locations;
using Android.Support.V4.App;
using Android.Telephony;
using System;
using System.Threading;

namespace OutdoorBuddy
{
    public class SmsSender
    {
        private const int REQUEST_SMS = 123;
        private const int REQUEST_PHONE_STATE = 123;

        private Activity Activity { get; set; }

        public SmsSender(Activity activity)
        {
            Activity = activity;
        }

        public void SendSms(Location location)
        {
            Monitor.Enter(this);

            try
            {
                if (!CanSendSms())
                {
                    RequestSmsPermissionsAndSend();
                    return;
                }

                var preferences = Application.Context.GetSharedPreferences(Defines.PreferencesName, Android.Content.FileCreationMode.Private);
                var phoneNumber = preferences.GetString(Defines.PhoneKey, string.Empty);

                if (string.IsNullOrEmpty(phoneNumber))
                    throw new Exception("Can't send message to empty phone number");

                var message = "Run safe, buddy! Current Location: Latitude :" + location.Latitude + " Longtitude :" + location.Longitude;

                SmsManager.Default.SendTextMessage(phoneNumber, null, message, null, null);
            }
            finally
            {
                Monitor.Exit(this);
            }
        }

        private bool CanSendSms()
        {
            if (Activity.CheckSelfPermission(Manifest.Permission.SendSms) != (int)Permission.Granted)
                return false;

            if (Activity.CheckSelfPermission(Manifest.Permission.ReadPhoneState) != (int)Permission.Granted)
                return false;

            return true;
        }

        private void RequestSmsPermissionsAndSend()
        {
            if (Activity.CheckSelfPermission(Manifest.Permission.SendSms) != (int)Permission.Granted)
            {
                ActivityCompat.RequestPermissions(Activity, new string[] { Manifest.Permission.SendSms }, REQUEST_SMS);
            }

            if (Activity.CheckSelfPermission(Manifest.Permission.ReadPhoneState) != (int)Permission.Granted)
            {
                ActivityCompat.RequestPermissions(Activity, new string[] { Manifest.Permission.ReadPhoneState }, REQUEST_PHONE_STATE);
            }
        }
    }
}