drop table locations;
create table locations(
  id number primary key not null,
  coordinates varchar2(5000) not null,
  indate timestamp not null
);

drop table panic_request_status;
create table panic_request_status(
  kz char(3),
  descr varchar(50)
);

insert into panic_request_status(kz, descr)
  values('NEU', 'Request läuft');

insert into panic_request_status(kz, descr)
  values('ABR', 'Request Abgebrochen');

drop table panic_requests;
create table panic_requests(
  id number primary key not null,
  indate timestamp not null,
  status char(3) not null,
  foreign key(status) references panic_request_status(kz)
);

drop table panic_request_x_location;
create table panic_request_x_location(
  panic_request_id number not null,
  location_id number not null,
  primary key(panic_request_id, location_id),
  foreign key(panic_request_id) references panic_requests(id),
  foreign key(location_id) references locations(id)
);