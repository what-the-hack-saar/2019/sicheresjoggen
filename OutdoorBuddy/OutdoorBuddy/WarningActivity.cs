﻿using Android;
using Android.App;
using Android.Content.PM;
using Android.Gms.Common;
using Android.Gms.Location;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Util;
using Android.Widget;
using System;
using System.Threading.Tasks;
using System.Timers;

namespace OutdoorBuddy
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class WarningActivity : AppCompatActivity
    {
        private TextInputEditText textInput;

        public Timer Timer { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_warning);

            Button okButton = FindViewById<Button>(Resource.Id.okButton);
            okButton.Click += onOkClick;

            Button stopRunning = FindViewById<Button>(Resource.Id.stopRunningFromWarning);
            stopRunning.Click += onStopRunningClick;

            Button alarmButton = FindViewById<Button>(Resource.Id.warningAlarm);
            alarmButton.Click += onAlarmButtonClick;

            Timer = new Timer(7000);
            Timer.AutoReset = false;
            Timer.Elapsed += new ElapsedEventHandler(HandleTimerElapsed);
            Timer.Start();
        }

        private async void HandleTimerElapsed(Object source, ElapsedEventArgs e)
        {
            LocationManager locationManager = new LocationManager(this);
            locationManager.GetLocationAndSendSms1();
        }

        private void onStopRunningClick(object sender, EventArgs e)
        {
            // Ask for PIN, go back to MainActivity
            throw new NotImplementedException();
        }

        private void onAlarmButtonClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void onOkClick(object sender, EventArgs e)
        {
            // TODO: Ask for PIN

            Timer.Stop();

            StartActivity(typeof(RunningActivity));
        }

        private void OnSaveButtonClicked(object sender, EventArgs e)
        {
            var contextPref = Application.Context.GetSharedPreferences(Defines.PreferencesName, Android.Content.FileCreationMode.Private);
            var contextEdit = contextPref.Edit();
            contextEdit.PutString(Defines.PhoneKey, textInput.Text);
            contextEdit.Commit();

            // TODO step back or Toast or both
        }
    }
}

