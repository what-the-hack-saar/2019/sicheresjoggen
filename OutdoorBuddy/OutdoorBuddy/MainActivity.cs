﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using System;

namespace OutdoorBuddy
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            Button startButton = FindViewById<Button>(Resource.Id.startRunning);
            startButton.Click += onStartRunningClick;
        }

        private void onStartRunningClick(object sender, EventArgs e)
        {
            UserState.getState().setRunningState("Freier Lauf");
            StartActivity(typeof(RunningActivity));
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                StartActivity(typeof(SettingsActivity));
                return true;
            }
            else if (id == Resource.Id.action_routes )
            {
                StartActivity(typeof(RoutesActivity));
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}