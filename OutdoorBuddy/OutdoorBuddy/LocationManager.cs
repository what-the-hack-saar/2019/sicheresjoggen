﻿using Android;
using Android.App;
using Android.Content.PM;
using Android.Gms.Common;
using Android.Gms.Location;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Util;
using System;
using System.Threading.Tasks;
using System.Timers;

namespace OutdoorBuddy
{
    public class LocationManager
    {
        private Activity Activity { get; set; }

        public LocationManager(Activity activity)
        {
            Activity = activity;
        }

        public async void GetLocationAndSendSms1()
        {
            if (IsGooglePlayServicesInstalled())
            {
                while ((ContextCompat.CheckSelfPermission(Activity, Manifest.Permission.AccessFineLocation) != Permission.Granted))
                {
                    ActivityCompat.RequestPermissions(Activity, new string[] { Manifest.Permission.AccessFineLocation }, 1);
                }
            }

            await GetLocationAndSendSms();
        }

        private bool IsGooglePlayServicesInstalled()
        {
            var queryResult = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(Activity);
            if (queryResult == ConnectionResult.Success)
            {
                Log.Info("RunningActivity", "Google Play Services is installed on this device.");
                return true;
            }

            if (GoogleApiAvailability.Instance.IsUserResolvableError(queryResult))
            {
                var errorString = GoogleApiAvailability.Instance.GetErrorString(queryResult);
                Log.Error("RunningActivity", "There is a problem with Google Play Services on this device: {0} - {1}",
                          queryResult, errorString);
            }

            return false;
        }

        private async Task GetLocationAndSendSms()
        {
            var fusedLocationProviderClient = LocationServices.GetFusedLocationProviderClient(Activity);

            var lastLocation = await fusedLocationProviderClient.GetLastLocationAsync();

            var sender = new SmsSender(Activity);
            sender.SendSms(lastLocation);
        }
    }
}