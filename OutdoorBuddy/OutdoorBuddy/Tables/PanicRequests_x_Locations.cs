﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OutdoorBuddy
{
    class PanicRequests_x_Locations
    {
        public int panicRequestId { get; set; }
        public int locationId { get; set; }
    }
}