﻿using System;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content.PM;
using Android.Gms.Common;
using Android.Gms.Location;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Telephony;
using Android;
using Android.Content.PM;
using System.Globalization;

namespace OutdoorBuddy
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class RoutesActivity : AppCompatActivity
    {
        private TextInputEditText textInputAlarmContact;
        private TextInputEditText textInputGoodPin;
        private TextInputEditText textInputBadPin;

        private const String phoneKey = "phone_key";
        private const String goodPinKey = "goodpin_key";
        private const String badPinKey = "badpin_key";


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_routes);

            var contextPref = Application.Context.GetSharedPreferences("OutdoorBuddy", Android.Content.FileCreationMode.Private);

            Button buttonAddRoute = FindViewById<Button>(Resource.Id.buttonAddRoute);
            buttonAddRoute.Click += onAddNewRoute;
        }

        private void onAddNewRoute(object sender, EventArgs e)
        {
            var inputDialog = new Android.App.AlertDialog.Builder(this);
            EditText userInput = new EditText(this);

            String routeName = "";

            string selectedInput = string.Empty;
            userInput.Hint = "Routenname";
            //userInput.Text = "Routenname"; // GetSavedInput("InputText", out selectedInput);
            //SetEditTextStylings(userInput);
            //userInput.InputType = Android.Text.InputTypes.NumberFlagDecimal | Android.Text.InputTypes.ClassNumber;
            inputDialog.SetTitle(selectedInput);
            inputDialog.SetView(userInput);
            inputDialog.SetPositiveButton(
                "Ok",
                (see, ess) =>
                {
                    if (userInput.Text == string.Empty )
                    {
                        return;
                    }
                    routeName = userInput.Text;
                                        
                    UserState.getState().setRunningState("Route aufzeichnen: "+routeName);
                    StartActivity(typeof(RunningActivity));
                });
            inputDialog.SetNegativeButton("Abbrechen", 
                (afk, kfa) => { return; });
            inputDialog.Show();
            //ShowKeyboard(userInput);

            // TODO ask for name first
            
        }

        private void onAbortButtonClicked(object sender, EventArgs e)
        {
            Finish();
        }

        public static String getGoodPin()
        {
            var contextPref = Application.Context.GetSharedPreferences("OutdoorBuddy", Android.Content.FileCreationMode.Private);
            return contextPref.GetString(goodPinKey, "");
        }

        public static String getBadPin()
        {
            var contextPref = Application.Context.GetSharedPreferences("OutdoorBuddy", Android.Content.FileCreationMode.Private);
            return contextPref.GetString(badPinKey, "");
        }

        private void OnSaveButtonClicked(object sender, EventArgs e)
        {
            var contextPref = Application.Context.GetSharedPreferences("OutdoorBuddy", Android.Content.FileCreationMode.Private);
            var contextEdit = contextPref.Edit();
            contextEdit.PutString(phoneKey, textInputAlarmContact.Text);
            contextEdit.PutString(goodPinKey, textInputGoodPin.Text);
            contextEdit.PutString(badPinKey, textInputBadPin.Text);
            contextEdit.Commit();

            Finish();
        }
    }
}

